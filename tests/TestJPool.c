
#include "../spaceinvaders/JBase.h"
#include "../spaceinvaders/JPool.h"
#include <glib.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

/*============================================================================*/
int main(){
    JPool* p = JPool_New(32);
    int* e;

    /* get empty */
    assert("Pop da pool vuoto: " && JPool_Pop(p)==NULL);

    /* fill at max size returns always something >=0 */
    for (int i=0; i<p->size; i++){
        e = malloc(sizeof(int));
        *e = i;
        assert("Push to fill " && JPool_Push(p,e)>=0);
    }

    /* trying to add a new element returs -1 */
    assert("Pushing over size" && JPool_Push(p,e)<0);

    /* pops all elements is ok */
    for (int i=0; i<p->size; i++){
        assert("Popping a null" && JPool_Pop(p)!=NULL);
    }

    /* now size is equals to head + 1*/
    assert("Error head+1 != size" && p->size == p->head+1);

    /* popping from empty returns NULL */
    assert("Error: second pop from empty pool" && JPool_Pop(p)==NULL);

    printf("\n=== SUCCESS ===\n");
    /**/
    JPool_FreeAll(p,free);
}
