
#pragma once

#include <glib.h>
#include <string.h>

#include <libxml/parser.h>
#include <libxml/tree.h>

#include "JBase.h"
#include "Entity.h"

#define MODEL_MAX_ENTITIES 20
#define MODEL_BUFF_LENGTH 32

/** 
 * Entity_template
 * A lovely Entity extension used to store some metainformation usefull to
 * optimize entities references on ia, bullets and side
 *
 * @param g_str_hash 
 * @param g_str_equal 
 * 
 * @return 
 */
typedef struct{
    struct Entity_struct;
    char name[MODEL_BUFF_LENGTH];
    char ai_routine_name[MODEL_BUFF_LENGTH];
    char bullet_id_name[MODEL_BUFF_LENGTH];
    char side_name[MODEL_BUFF_LENGTH];
}Entity_template;

/** 
 * Model
 * Container for program I/O operations
 *
 * @param g_str_hash 
 * @param g_str_equal 
 * 
 * @return 
 */
typedef struct{
    /* entities fields */
    Entity_template* entities_templates[MODEL_MAX_ENTITIES];
    int num_templates;
    GHashTable* template_ids;
}Model;

/** 
 * Model_Init
 * 
 * @param model 
 * @param entities_db_path 
 * 
 * @return 
 */
Model* Model_Init(Model* model, char* entities_db_path);

/** 
 * Model_LoadTemplates
 * 
 * @param model 
 * @param entities_database 
 */
void Model_LoadTemplates(Model* model, char* entities_database);


/** 
 * Model_GetEntityCode
 * Get an Entity code by its name
 *
 * @param model 
 * @param entity_name 
 * 
 * @return 
 */
GLuint Model_GetEntityCode(Model* model, char* entity_name);
/** 
 * 
 * 
 * @param model 
 * @param template_id 
 * @param dest 
 * 
 * @return 
 */
Entity* Model_InitEntity(Model* model, GLuint template_id, Entity* dest);

GLuint Model_GetSize(Model* m);

/** 
 * Model_Delete
 * 
 * @param model 
 */
void Model_Delete(Model* model);
