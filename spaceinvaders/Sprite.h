
#pragma once

#include "SDL2/SDL.h"
#include "SDL2/SDL_opengl.h"

/*
 * A graphical element on the screen, mapped from an image atlas and drawn
 * at specific position. Its dimensions are
*/

typedef struct{
    GLuint frames_number; /**< number of frames for this sprite */
    GLuint current_frame; /**< current frame to show */
    SDL_Rect* frames;     /**< position on image atlas */
    SDL_Rect draw_rect;   /**< where to draw current frame */
    GLfloat wh_ratio;     /**< image width-height ratio */
}Sprite;

/** 
 * Sprite_Init
 * 
 * @param sprite 
 * @param frames_number 
 * @param atlas_coordinates_xywh: coordinates of sprite's frames. They're in a
 * single array, stored sequentially
 * 
 * @return 
 */
Sprite* Sprite_Init(Sprite* sprite,
                    GLuint frames_number,
                    GLuint* atlas_coordinates_xywh);


/** 
 * Sprite_Resize
 * 
 * @param e 
 * @param newwidth 
 */
void Sprite_Resize(Sprite* sprite, GLuint newwidth);

/** 
 * Sprite_Resize_Ratio
 * Resize Sprite according to a world/screen ration. It express how many 
 * meters are necessary to fill a pixel on the screen
 * @param e 
 * @param ratio
 */
void Sprite_Resize_Ratio(Sprite* sprite, GLfloat ratio);

/** 
 * Entity_Move
 * Muove un'entità di un deltax e un deltay specificati
 * @param e 
 * @param deltax 
 * @param deltay 
 */
void Sprite_Move(Sprite *sprite, GLfloat newx, GLfloat newy);

/** 
 * Sprite_Draw
 * 
 * @param sprite 
 * @param renderer 
 */
void Sprite_Draw(Sprite* sprite, SDL_Renderer* renderer, SDL_Texture* image);

