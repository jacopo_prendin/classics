
#include "AIManager.h"
#include <assert.h>

GLboolean Entity_HitsBoundiaries(Entity* e, World* w){
    /* check screen boundaries */
    JPoint bl = JRect_BottomLeft(e->box);

    if ( bl.y >= w->size.height || e->box.position.y<=0)
        return GL_TRUE;
    else
        return GL_FALSE;
}

/* this is a "private" field ;) */
static AI_function_pointer *AIdatabase;

/** 
 * Example of ai routine
 * 
 * @param e 
 * @param w 
 * 
 * @return 
 */
GLboolean BugAIRoutine(Entity* e, World* w,GLfloat elapsed_time){
    /* continue descending */
    e->box.position.y += e->speed * elapsed_time/1000.0;
    JPoint bl = JRect_BottomLeft(e->box);

    /* check screen boundaries */
    if ( bl.y >= w->size.height) e->speed = -25.0;
    if ( e->box.position.y<=0) e->speed = 25.0;

    return GL_TRUE;
}

GLboolean StraightPlayerProjectile(Entity* e, World* w,GLfloat elapsed_time){
    e->box.position.y -= e->speed * elapsed_time/1000.0;

    if (Entity_HitsBoundiaries(e,w))
        e->life = -1;
    return GL_TRUE;
    
}
/** 
 * AIdatabase_Init
 * Initialize a directory of artificial intelligece routines
 * @param ai_directory 
 */
void AI_Init(char* ai_directory){
    int size = 2;
    AIdatabase = calloc(size,sizeof(AI_function_pointer));

    AIdatabase[0] = BugAIRoutine;
    AIdatabase[1] = StraightPlayerProjectile;
}

/** 
 * 
 * 
 * @param routine_named 
 * 
 * @return 
 */
GLuint AI_GetRoutineNamed(char* routine_name){
    if (strcmp(routine_name,"Straight")==0)
        return 1;
    if (strcmp(routine_name,"Bug")==0)
        return 0;

    return 0;
}
/** 
 * AI_Execute
 */
GLboolean AI_Execute(Entity* e, World* w, GLfloat elapsed_time){
    GLboolean (*f)(Entity*,World*,GLfloat) = AIdatabase[e->ai_routine];
    return f(e,w,elapsed_time);
}

/**
 * AI_Quit
 */
void AI_Quit(){
    free(AIdatabase);
}
