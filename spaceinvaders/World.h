
#pragma once

#ifdef __APPLE__ 
    #include <OpenGL/gl.h>
#else
    #include <GL/gl.h>
#endif

#include <stdio.h>

#include <glib.h>

#include "JBase.h"
#include "JPool.h"
#include "Entity.h"
#include "Model.h"

#define WORLD_MAX_ENTITIES 128
#define ENTITY_EMPTY_SLOT -1

typedef struct{
    GLuint start_mills;
    GLboolean active;
}JTimer;

/**
 * World
 * Class to manage game logic
 */
typedef struct{
    GLuint last_milliseconds;
    JSize size;         /**< world's size */
    Entity player;      /**< special entity outside the others */

    GLboolean goUp,goDown,goLeft,goRight,Fire;  /**< buffer to keys status */

    Model* model;       /**< I/O routines */
    GLfloat timer_ms;   /**< store for timers */
    GLuint total_active_entities;  /**< active entities in pool */
    Entity** entities_pool;        /**< entities pool */
}World;

/** 
 * 
 * 
 * @param world 
 * @param milliseconds 
 */
void World_Init(World* world,
                GLfloat world_width_meters,
                GLfloat world_height_meters,
                char* entities_path);

/** 
 * World_Height_Meters
 * It should be useless in a C context, but... let's do it and see if we need
 * some logic
 * @param world 
 * 
 * @return 
 */
Entity* World_GetEntitiesBuffer(World* world);

/** 
 * 
 * 
 * @param world 
 * @param milliseconds 
 */
void World_Update(World* world, GLuint (*TimerFunc)(void) );

/** 
 * 
 * 
 * @param world 
 * @param position 
 * 
 * @return 
 */
JPoint* World_PlayerPosition(World* world, JPoint* position);

/** 
 * 
 * 
 * @param world 
 * 
 * @return 
 */
GLint World_PlayerLifeLevel(World* world);

/** 
 * World_Dealloc
 * Deallocate memory
 * @param w 
 */
void World_Dealloc(World* w);
