
#include "Model.h"

/** 
 * Model_Init
 * 
 * @param model 
 * @param entities_db_path 
 * 
 * @return 
 */
Model* Model_Init(Model* model, char* entities_db_path){
    memset(model->entities_templates,
           0,
           MODEL_MAX_ENTITIES);
    Model_LoadTemplates(model,entities_db_path);

    return model;
}

/** 
 * PrintEntityTemplate
 * A horrible debug function
 *
 * @param template 
 */
void PrintEntityTemplate(Entity_template* template){
    printf("%s\n",template->name);
    printf("\tw=%.1f h=%.1f\n",
           template->box.size.width,
           template->box.size.height);
    printf("\tSpeed%.1f\n",template->speed);
    printf("\tlife: %d\n",template->life);
    printf("\thit_damage: %d\n",template->hit_damage);
    printf("\tai_routine: %s\n",template->ai_routine_name);
    printf("\tside: %s\n",template->side_name);
    
    /* fire fields */
    printf("\tfire_id: %s\n",template->bullet_id_name);
    printf("\tlast_fire_update: %d\n",template->last_fire_update);
    printf("\tfire_mouth: %.1f %.1f\n",
           template->fire_mouth.x,
           template->fire_mouth.y);
    printf("\tfire_rate: %.1f\n",template->fire_rate);

}

/*
How an entity looks like: 
    - JRect box;  
    - GLfloat speed;
    - GLint life;
    - GLint hit_damage;
    - GLuint type;     
    - GLuint ai_routine;
    - GLuint tag;
    - GLuint side;
    - GLint impact_damages;
    - GLuint last_fire_update;
    - JPoint fire_mouth;
    - GLfloat fire_rate;
    - GLuint fire_id;
*/
#define XMLCHAR (const xmlChar*)

char* silycopy(char* d, xmlChar* src, int max){
    for (int i=0; i<MODEL_BUFF_LENGTH && src[i]!='\0'; i++)
        d[i]=src[i];

    return d;
}
/** 
 * _Model_GetAttr
 * Returns attribute named as name from node child, casting it to a boring
 * char*
 * @param child 
 * @param name 
 * 
 * @return 
 */
char* _Model_GetAttr(xmlNode* child, char* name){
    return (char*)xmlGetProp(child,(const xmlChar*)name);
}

/* convenience Macro to translate a if-tag statement  */
#define JFOR_TAG(expected_tag) if \
        (xmlStrEqual(node->name, \
                     (const xmlChar*)expected_tag))

/** 
 * _Model_LoadFire
 * Loads fire attributes on template entity
 * <fire id="bullet">
 *     <rate>0.5</rate>
 *     <mouth x="8.47" y="0"/>
 * </fire>
 *
 * @param m
 * @param doc 
 * @param firenode 
 * @param template 
 */
Entity_template* _Model_LoadFire(xmlDoc* doc,
                                 xmlNode* firenode,
                                 Entity_template* template){
    
    /* save id for fired entities */
    strncpy(template->bullet_id_name,
            _Model_GetAttr(firenode, "id"),
            MODEL_BUFF_LENGTH);

    /* save other parameters */
    xmlNode* node = firenode->xmlChildrenNode;
    xmlChar* fire_content;
    while (node){
        fire_content=xmlNodeListGetString(doc,
                                          node->xmlChildrenNode,
                                          1);
        JFOR_TAG("rate")
            template->fire_rate = atof((char*) fire_content);

        JFOR_TAG("mouth")
            template->fire_mouth=JPoint_make(
                                             atof(_Model_GetAttr(node,"x")),
                                             atof(_Model_GetAttr(node,"y")));

        node = node->next;
    }

    return template;
}
/** 
 * _Model_ParseEntity
 *
 * @param model 
 * @param doc 
 * @param entity 
 */
Entity_template* _Model_ParseEntity(Model* model,
                                    xmlDoc* doc,
                                    xmlNode* entity){
    xmlNode* node = entity->xmlChildrenNode;
    Entity_template* template;

    /* allocate and zeros a new entity template */
    template = malloc(sizeof(Entity_template));
    memset(template,0,sizeof(Entity_template));

    strncpy(template->name,
            (void*)xmlGetProp(entity,XMLCHAR"name"),
            MODEL_BUFF_LENGTH);

    /* scans all nodes */
    while(node){
        /* ignore comments */
        if (node->type==XML_COMMENT_NODE){
            node=node->next;
            continue;
        }

        xmlChar* content=xmlNodeListGetString(doc,
                                              node->xmlChildrenNode,
                                              1);
        /* parse node. I *LOVE* this macro I wrote :) */
        JFOR_TAG("speed")
            template->speed = atof((const char*)content);
        
        JFOR_TAG("box")
            template->box = JRect_make(0.0f,
                                       0.0f,
                                       atof(_Model_GetAttr(node,"width")),
                                       atof(_Model_GetAttr(node,"height"))
                                       );

        JFOR_TAG("life") template->life = atoi( (char*) content);

        JFOR_TAG("hit_damage") template->hit_damage = atoi((char*)content);

        JFOR_TAG("ai")
            strncpy(template->ai_routine_name,
                    (void*)content,
                    MODEL_BUFF_LENGTH);

        JFOR_TAG("tag") template->tag = 0;

        JFOR_TAG("side"){
            strncpy(template->side_name,
                    (void*) content,
                    MODEL_BUFF_LENGTH);
            
            /* define side */
            if (strcmp(template->side_name,"SIDE_PLAYER")==0)
                template->side = SIDE_PLAYER;
            if (strcmp(template->side_name,"SIDE_INVADERS")==0)
                template->side = SIDE_INVADERS;
            if (strcmp(template->side_name,"SIDE_POWERUPS")==0)
                template->side = SIDE_POWERUPS;
        }
        template->last_fire_update=0;

        JFOR_TAG("fire")
            _Model_LoadFire(doc, node, template);

        if (content!=NULL) free(content);
        node=node->next;
    }

    return template;
}

/** 
 * Model_LoadTemplates
 */
void Model_LoadTemplates(Model* model, char* entities_database){
    xmlDoc *doc = NULL;
    xmlNode *root_element = NULL;
    model->template_ids = g_hash_table_new(g_int_hash, g_str_equal);
    /*
     * this initialize the library and check potential ABI mismatches
     * between the version it was compiled for and the actual shared
     * library used.
     */
    LIBXML_TEST_VERSION

        /* either this son of a b**** counts spaces as text */
        xmlKeepBlanksDefault (0);  
    /*parse the file and get the DOM */
    doc = xmlParseFile(entities_database);

    if (doc == NULL) {
        printf("Model: could not parse XML\n");
        exit(1);
    }
    
    
    /* Get root element */
    root_element = xmlDocGetRootElement(doc);
    printf("Root name: %s\n",root_element->name);
    
    /* get entitis childs: looks for "channel" */
    xmlNode* node = root_element->xmlChildrenNode;
    int index, *newid;
    index=0;
    Entity_template* new_template;
    while(node){
        if (node->type!=XML_COMMENT_NODE)
            /* create a new template and save it on the array */
            JFOR_TAG("entity"){
                new_template=_Model_ParseEntity(model, doc, node);
                model->entities_templates[index]=new_template;
                newid = malloc(sizeof(int));
                *newid = index;
                new_template->type=index;
                g_hash_table_insert(model->template_ids,
                                    new_template->name,
                                    newid);
                index++;
            }
                
        node=node->next;
    }

    model->num_templates=index;
    /* all entities_template are loaded: "compile"
     *  a. fired entities
     *  b. AI routines
     *  c. Side
     */
    int* code;
    for (int i=0; i<index; i++){
        if (index>=MODEL_MAX_ENTITIES){
            printf("Error: entities templates more than available space\n"
                   "Model->Model_LoadTemplates\n");
            exit(1);
        }
        Entity_template* tmpl = model->entities_templates[i];

        /* if this template can fire, find id's of fired entity */
        if (strlen(tmpl->bullet_id_name)>0){
            code = g_hash_table_lookup(model->template_ids,
                                       tmpl->bullet_id_name);
            tmpl->fire_id=*code;
        }
    }

    /* free memory */
    xmlFreeDoc(doc);
}

/** 
 * Model_InitEntity
 */
Entity* Model_InitEntity(Model* model, GLuint template_id, Entity* dest){
    Entity_template* e = model->entities_templates[template_id];

    dest->box   = e->box;
    dest->speed = e->speed;
    dest->life  = e->life;
    dest->type  = e->type;
    dest->side  = e->side;

    dest->fire_id     = e->fire_id;
    dest->fire_mouth  = e->fire_mouth;
    dest->fire_rate   = e->fire_rate; 

    dest->last_fire_update = 0;
    dest->ai_routine       = e->ai_routine;
    dest->hit_damage       = e->hit_damage;

    //printf("Entity %s init with life=%d\n",e->name,dest->life);
    return dest;
}

/** 
 * Model_GetSize
 */
GLuint Model_GetSize(Model* m){
    return m->num_templates;
}

/** 
 * Model_GetEntityCode
 */
GLuint Model_GetEntityCode(Model* m, char* entity_name){
    int* code = g_hash_table_lookup(m->template_ids,entity_name);
    return *code;
}
/** 
 * Model_Delete
 */
void Model_Delete(Model* m){
    g_hash_table_destroy(m->template_ids);
    free(m);
}
