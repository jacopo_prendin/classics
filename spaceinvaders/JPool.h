
#pragma once

#include <stdlib.h>
#include "JBase.h"

typedef void* JPointer;

typedef struct{
    GLint size;
    GLint head;     /**< index of first free slot */
    JPointer* pool;
}JPool;

JPool* JPool_New(GLint size);

JPointer JPool_Pop(JPool* jpool);

GLint JPool_Push(JPool* jpool, JPointer e);

void JPool_FreeAll(JPool* jpool, void(*DeleteFunc)(JPointer));
