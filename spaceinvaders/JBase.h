
#pragma once

#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#ifdef __APPLE__ 
    #include <OpenGL/gl.h>
#else
    #include <GL/gl.h>
#endif

GLuint SCREEN_WIDTH,SCREEN_HEIGHT;

/**
 * Punto generico nello spazio 3d
 */
typedef struct JPoint{
     GLfloat x, y,z;
}JPoint;

typedef struct JSize{
     GLfloat width, height, depth;
}JSize;

typedef struct JRect{
     JPoint position;
     JSize size;
}JRect;


typedef struct JColor{
     GLfloat red,green,blue,alpha;
}JColor;

typedef struct JRange{
    GLfloat start,end;
}JRange;


JPoint JPoint_make(GLfloat x, GLfloat y);

JPoint JPoint_make3f(GLfloat x, GLfloat y, GLfloat z);

/** 
 * 
 * 
 * @param x 
 * @param y 
 * @param width 
 * @param height 
 * 
 * @return 
 */       
JRect JRect_make(GLfloat x, GLfloat y, GLfloat width, GLfloat height);

/** 
 * 
 * 
 * @param rect 
 * @param x 
 * @param y 
 * @param width 
 * @param height 
 */
JRect* JRect_init(JRect* rect, GLfloat x, GLfloat y, GLfloat width, GLfloat height);

GLboolean JRect_Overlaps(JRect rect1, JRect rect2);

JPoint JRect_BottomLeft(JRect rect);

JPoint JRect_BottomRight(JRect rect);

JRect JRect_make3d(GLfloat x, GLfloat y, GLfloat z, GLfloat width, GLfloat height);

JColor JColor_make(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha);

/** 
 * 
 * 
 * @param color 
 * @param red 
 * @param green 
 * @param blue 
 * @param alpha 
 */
JColor* JColor_init(JColor* color, GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha);

/** 
 * JBase_Distance
 * 
 * @param temp 
 * @param position 
 * 
 * @return 
 */
GLfloat JBase_Distance(JPoint a, JPoint b);

GLfloat JRandFloat(GLfloat start, GLfloat end);

/** 
 * JBase_Chop
 * Rimuove il carattere "a capo" da una stringa sostituendo \n con \0
 * 
 * @param string 
 * 
 * @return 
 */
char* JBase_Chop(char* string);
