
#include "Sprite.h"

/** 
 * Sprite_Init
 */
Sprite* Sprite_Init(Sprite* sprite,
                    GLuint frames_number,
                    GLuint* atlas_coordinates_xywh){

    sprite->frames_number = frames_number;
    sprite->current_frame = 0;

    sprite->frames = calloc(frames_number, sizeof(SDL_Rect));
    
    /* stores all frames */
    for (int i=0; i<frames_number; i++){
        sprite->frames[i].x=atlas_coordinates_xywh[i];
        sprite->frames[i].y=atlas_coordinates_xywh[i+1];
        sprite->frames[i].w=atlas_coordinates_xywh[i+2];
        sprite->frames[i].h=atlas_coordinates_xywh[i+3];
    }

    SDL_Rect first=sprite->frames[0];
    sprite->draw_rect.x=first.x;
    sprite->draw_rect.y=first.y;
    sprite->draw_rect.w=first.w;
    sprite->draw_rect.h=first.h;

    /* calculate frames ratio */
    sprite->wh_ratio = (GLfloat)first.w / (GLfloat)first.h;

    return sprite;
}


/** 
 * Sprite_Resize
 */
void Sprite_Resize(Sprite* sprite, GLuint newwidth){
    sprite->draw_rect.w = newwidth;
    sprite->draw_rect.h = sprite->draw_rect.w/sprite->wh_ratio;
}

/** 
 * Sprite_Resize_Ratio
 */
void Sprite_Resize_Ratio(Sprite* sprite, GLfloat ratio){
    printf("Sprite_Resize_Ratio: from: ratio=%.5f m/p %d,%d to %.4f,%.4f\n",
           ratio,
           sprite->draw_rect.w,
           sprite->draw_rect.h,
           sprite->draw_rect.w / ratio,
           sprite->draw_rect.h / ratio);

    sprite->draw_rect.w /= ratio;
    sprite->draw_rect.h /= ratio;
}
/** 
 * Entity_Move
 */
void Sprite_Move(Sprite *sprite, GLfloat newx, GLfloat newy){
    sprite->draw_rect.x=newx;
    sprite->draw_rect.y=newy;
}

/** 
 * Sprite_Draw
 */
void Sprite_Draw(Sprite* sprite, SDL_Renderer* renderer,  SDL_Texture* atlas){
    SDL_RenderCopy( renderer, atlas,
                    &(sprite->frames[sprite->current_frame]),
                    &(sprite->draw_rect));
}
