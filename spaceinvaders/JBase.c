
#include "JBase.h"

/** 
 * JPoint_new
 */
JPoint JPoint_make(GLfloat x, GLfloat y){
    JPoint p;

    p.x = x;
    p.y = y;
    p.z = 0.0f;

    return p;
}

JPoint JPoint_make3f(GLfloat x, GLfloat y, GLfloat z){
    JPoint p = {.x = x, .y = y, .z = z};
    return p;
}
/** 
 * 
 */
JRect JRect_make(GLfloat x, GLfloat y, GLfloat width, GLfloat height){
     JRect rect;

     rect.position.x = x;
     rect.position.y = y;
     rect.size.width = width;
     rect.size.height = height;

     return rect;
}

/** 
 * 
 */
JRect JRect_make3d(GLfloat x, GLfloat y, GLfloat z, GLfloat width, GLfloat height){
    JRect rect = JRect_make(x,y,width,height);
    rect.position.z = z;
    return rect;
}
/**
 *
 */
JRect* JRect_init(JRect* rect, GLfloat x, GLfloat y, GLfloat width, GLfloat height){
     rect->position.x = x;
     rect->position.y = y;
     rect->size.width = width;
     rect->size.height = height;

     return rect;
}

/** 
 * JRect_Overlaps
 */
GLboolean JRect_Overlaps(JRect rect1, JRect rect2){
    if (
        // rect1 upper left corner falls inside rect2
        (rect1.position.x <  rect2.position.x + rect2.size.width &&
         rect1.position.x >= rect2.position.x &&
         rect1.position.y <  rect2.position.y + rect2.size.height &&
         rect1.position.y >= rect2.position.y) ||

        // rect2 upper corner falls inside rect1
        (rect2.position.x <  rect1.position.x + rect1.size.width &&
         rect2.position.x >= rect1.position.x &&
         rect2.position.y <  rect1.position.y + rect1.size.height &&
         rect2.position.y >= rect1.position.y))
        
        return GL_TRUE;

    return GL_FALSE;
}


/** 
 * JRect_BottomLeft
 */
JPoint JRect_BottomLeft(JRect rect){
    
    return JPoint_make(rect.position.x,
                       rect.position.y + rect.size.height);
}

/** 
 * JRect_BottomRight
 */

JPoint JRect_BottomRight(JRect rect){
    return JPoint_make(rect.position.x + rect.size.width,
                       rect.position.y + rect.size.height);
}

/** 
 * JColor_make
 * 
 * @param red 
 * @param green 
 * @param blue 
 * @param alpha 
 */
JColor JColor_make(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha){
    JColor color;
    color.red = red;
    color.green = green;
    color.blue = blue;
    color.alpha = alpha;
    return color;
}

/**
 *
 */
JColor* JColor_init(JColor* color, GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha){
     color->red = red;
     color->green = green;
     color->blue = blue;
     color->alpha = alpha;
     return color;
}

/** 
 * 
 * 
 * @param a 
 * @param b 
 * 
 * @return 
 */
GLfloat JBase_Distance(JPoint a, JPoint b){
     return sqrt(
          pow(a.x - b.x,2) + 
          pow(a.y - b.y,2)
          );
}


/** 
 * JRandFloat
 */
GLfloat JRandFloat(GLfloat start, GLfloat end){
    double norm = (double)rand() / (double)RAND_MAX;
    GLfloat range = end - start;

    return start+(norm*range);
}

/** 
 * JBase_Chop
 */
char* JBase_Chop(char* string){
    for (int fnl=0; fnl<strlen(string); fnl++)
        if (string[fnl]=='\n'){
            string[fnl]='\0';
            break;
        }
    return string;
}
