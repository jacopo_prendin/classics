
#include "World.h"
#include "AIManager.h"
/** 
 * 
 * 
 * @param e 
 * @param entity_template_id 
 * @param start 
 * 
 * @return 
 */

Entity* Entity_Init(Model* model, Entity* e,
                    GLuint entity_template_id,
                    JPoint start){

    /* simply, an "empty slot" is just an entity with life
     * setted to a value less than zero
    */
    if (entity_template_id == ENTITY_EMPTY_SLOT){
        e->life=-1;
        return e;
    }

    e=Model_InitEntity(model,entity_template_id,e);

    /* specific initialization */
    e->type = entity_template_id;
    e->last_fire_update=0;
    e->box.position.x=start.x;
    e->box.position.y=start.y;

    return e;
}

/** 
 * World_Init
 */
void World_Init(World* world,
                GLfloat world_width_meters,
                GLfloat world_height_meters,
                char* entities_path){

    /* Initialize fundamental world's members */
    world->last_milliseconds=-1;
    world->size.width=world_width_meters;
    world->size.height=world_height_meters;
    world->total_active_entities=0;

    /* initialize Model */
    world->model = Model_Init(malloc(sizeof(Model)),
                              entities_path);

    /* initialize artificial intelligence system */
    AI_Init("w figa");

    /* assign code to every entity's ai */
    Entity_template* tmpl;
    for (int i=0; i<Model_GetSize(world->model); i++){
        tmpl = world->model->entities_templates[i];
        tmpl->ai_routine = AI_GetRoutineNamed(tmpl->ai_routine_name);
    }

    /* initialize player */
    int p_id=Model_GetEntityCode(world->model,
                                 "Player");
    Entity_Init(world->model,
                &(world->player),
                p_id,
                JPoint_make(world->size.width/2,
                            world->size.height-32.0)
                );
    world->player.tag=292929;

    /* comandi da tastiera */
    world->goUp = GL_FALSE;
    world->goDown = GL_FALSE;
    world->goLeft = GL_FALSE;
    world->goRight = GL_FALSE;
    world->Fire = GL_FALSE;
    
    /* initialize entities */
    JPoint start = {.x = 0, .y=-5};
    world->entities_pool=calloc(WORLD_MAX_ENTITIES,sizeof(Entity*));
    for (int i=0; i<WORLD_MAX_ENTITIES; i++){
        Entity* e = malloc(sizeof(Entity));
        e = Entity_Init(world->model, e,
                        ENTITY_EMPTY_SLOT, start);
        world->entities_pool[i]=e;
    }

    /* timer */
    world->timer_ms=-1;
}

/** 
 * _World_NewEntity
 * Brutally search for a new free location in entities pool
 * @param w 
 * 
 * @return 
 */
Entity* _World_NewEntity(World* w){
    Entity* e=NULL;
    for (int i=0; i<WORLD_MAX_ENTITIES; i++){
        e=w->entities_pool[i];
        if (e->life<0)
            return e;
    }
    return NULL;
}
/** 
 * 
 * 
 * @param e 
 * @param w 
 * @param current_millis 
 */
void Entity_ShootInWorld(Entity* e, World* w, GLfloat current_millis){
    /* can entity shoot another projectile? */
    GLfloat elapsed_seconds = (current_millis - e->last_fire_update)/1000.0;
    if ( elapsed_seconds < e->fire_rate){
        return;
    }

    /* ok! now shots! */
    Entity* projectile = _World_NewEntity(w);
    if (projectile == NULL){ 
        printf("World->Entity_ShootInWorld: can't obtain a new entity"
               " from pool\n"
               " exit(1)\n");
        exit(1);
    }

    Entity_Init(
                w->model,
                projectile,
                e->fire_id, 
                JPoint_make(e->box.position.x + e->fire_mouth.x,
                            e->box.position.y + e->fire_mouth.y));

    /* always shoot something by your side :D */
    projectile->side = e->side;
    e->last_fire_update = current_millis;
}

/** 
 * _Entity_CheckCollision
 * 
 * @param e 
 * @param other 
 */
void _Entity_CheckCollision(Entity* e, Entity* other){
    if (other->life<0 || e->side==other->side)
        return;

    /* damage entities with their reciprocal hit_damage */
    if (JRect_Overlaps(e->box, other->box)){
        if (other->type==2){
            printf("_Entity_CheckCollision: with %d [%d] bullet=[%d]",
                   e->tag,
                   e->side,
                   other->side);
        }
        e->life -= other->hit_damage;
        other->life -= e->hit_damage;
    }
}

/** 
 * World_Update
 * 
 * @param world 
 * @param TimerFunc 
 */
void World_Update(World* world, GLuint (*TimerFunc)(void) ){
    GLuint current_millis = TimerFunc();
    GLfloat elapsed_time=current_millis - world->last_milliseconds;
    Entity* player = &(world->player);
    GLfloat speed = player->speed;

    /* manage players' movements */
    if (world->goUp)    player->box.position.y -= speed * elapsed_time/1000.0;
    if (world->goDown)  player->box.position.y += speed * elapsed_time/1000.0;
    if (world->goLeft)  player->box.position.x -= speed * elapsed_time/1000.0;
    if (world->goRight) player->box.position.x += speed * elapsed_time/1000.0;

    /* manage fire events from player */
    if (world->Fire)
        Entity_ShootInWorld(&(world->player),
                            world,
                            current_millis);
    /* check boundiaries */
    if (player->box.position.y <0)
        player->box.position.y = 0;
    
    if (player->box.position.y > world->size.height - player->box.size.height)
        player->box.position.y = world->size.height - player->box.size.height;
    
    if (player->box.position.x < 0 )
        player->box.position.x = 0;
    
    if (player->box.position.x >= world->size.width - player->box.size.width)
        player->box.position.x = world->size.width - player->box.size.width;
    

    /* update all active entities in the list */
    Entity* current, *other;
    world->total_active_entities=0;

    for (int i=0; i<WORLD_MAX_ENTITIES-1; i++){
        current = world->entities_pool[i];
        /* ignores _dead_ entities */
        if (current->life < 0){
            continue;
        }

        /* active entities works just for enemies */
        if (current->side != SIDE_PLAYER)
            world->total_active_entities++;

        /* check collisions with player */
        _Entity_CheckCollision(&(world->player), current);

        /* hard-wired collision detection or out of bounds. Sets life to
         * a value less-than-zero if necessary
         */
        for (int j=i+1; j<WORLD_MAX_ENTITIES; j++){
            other = world->entities_pool[j];
            _Entity_CheckCollision(current,other);

            /* if current or other have NOW a less-than-zero life, notify their
             * death to world event queue 
            */
            /* if (current->life < 0)*/
            /*     World_EventsQueueAdd(world, */
            /*                          UNIT_DIES_AT, */
            /*                          current->box.position); */

            /* if (other->life < 0)*/
            /*     World_EventsQueueAdd(world, */
            /*                          UNIT_DIES_AT, */
            /*                          other->box.position); */


        }

        /* execute AI routines for current entity */
        AI_Execute(current, world, elapsed_time);

        /* if (player->life < 0) */
        /*     World_EventsQueueAdd(events, */
        /*                          PLAYER_DIES_AT, */
        /*                          world->player->box.position); */
    }

    /* is necessary to reload units */
    if ( world->total_active_entities==0){

        JPoint start={.x = 10, .y = -5};
        Entity* e;
        world->total_active_entities = 10;

        GLint unit = Model_GetEntityCode(world->model,"Bug");

        for (int i=0; i<world->total_active_entities; i++){
            e = _World_NewEntity(world);
            Entity_Init( world->model, e, unit, start);
            e->tag = i;
            start.x += 10;
        }
        /* stops timer */
        world->timer_ms = -1;
    }

    world->last_milliseconds=TimerFunc();
}

/** 
 * World_PlayerPosition
 */
JPoint* World_PlayerPosition(World* world, JPoint* position){
    position->x = world->player.box.position.x;
    position->y = world->player.box.position.y;
        
    return position;
}

/**
 * World_Dealloc
 */
void World_Dealloc(World* w){
    for (int i=0; i<WORLD_MAX_ENTITIES; i++)
        free(w->entities_pool[i]);

    /* clear entities in pool */
    free(w->entities_pool);

    /* free model */
    Model_Delete(w->model);

    /* quit and free artificial intelligence system */
    AI_Quit();
}
