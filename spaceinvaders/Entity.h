
#pragma once

/* Who do you belong? */
#define SIDE_PLAYER 1
#define SIDE_INVADERS 2
#define SIDE_POWERUPS 3

/**
 * Entity
 * Active living object inside the World
 */
typedef struct Entity_struct{
    JRect box;              /**< biggest bounding box */
    GLfloat speed;
    GLint life;
    GLint hit_damage;       /**< damages inflicted to other entities when hit */
    GLuint type;            /**< Entity type */
    GLuint ai_routine;      /**< which type of AI routine drives the entity */
    GLuint tag;             /**< a numeric tag. Sometimes helps debug */
    GLuint side;            /**< are you a friend of a enemy? */
    /* fire fields */
    GLuint fire_id;         /**< projectile's entity type */
    GLuint last_fire_update;/**< stores last time you shoot */
    JPoint fire_mouth;      /**< from which hole you shoot? */
    GLfloat fire_rate;      /**< how many projectile you can shoot in a second*/
}Entity;
