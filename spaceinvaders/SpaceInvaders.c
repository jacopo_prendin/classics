
#include <string.h>
#include <glib.h>

#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"

#include "SDL2/SDL_opengl.h"


#include "World.h"
#include "Sprite.h"

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600

//The window we'll be rendering to
static SDL_Window* window = NULL;

//SDL Renderer
static SDL_Renderer *renderer;

//The image we will load and show on the screen
static SDL_Texture* image_atlas = NULL;

static char *BASE_PATH = NULL;

static Sprite player;
static Sprite bug;
static Sprite projectile;
static GLfloat PIXEL_PER_METER = 1;

/** 
 * InitializeSDL:
 * 
 */
void InitializeSDL(){
    BASE_PATH=SDL_GetBasePath();
    if( SDL_Init( SDL_INIT_VIDEO ) < 0 ){
        printf( "SDL could not initialize! SDL_Error: %s\n", SDL_GetError() );
        exit(1);
    }

    //Create window
    window = SDL_CreateWindow( "Days of '71",
                               SDL_WINDOWPOS_UNDEFINED,
                               SDL_WINDOWPOS_UNDEFINED,
                               SCREEN_WIDTH, SCREEN_HEIGHT,
                               SDL_WINDOW_SHOWN );
    if( window == NULL ) {
        printf( "Window could not be created! SDL_Error: %s\n",
                SDL_GetError() );
        exit(1);
    }

    /* create renderer */
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    if (renderer == NULL){
        printf("Can't create renderer\n");
        exit(1);
    }

    //Initialize PNG loading
    int imgFlags = IMG_INIT_PNG;
    if( !( IMG_Init( imgFlags ) & imgFlags ) ) {
        printf( "SDL_image could not initialize! SDL_image Error: %s\n",
                IMG_GetError() );

        exit(1);
    }
}
/** 
 * LoadMedia
 * Carica immagini e suoni
 */
void LoadMedia(){
    SDL_SetRenderDrawColor(renderer, 10, 10, 50, 255);
    char imgpath[512];
    memset(imgpath,'\0',512);

    strncat(imgpath,BASE_PATH,512);
    strncat(imgpath,"sprite_atlas.png",512);
    SDL_Surface* surface = IMG_Load(imgpath);

    //Create texture from surface pixels
    image_atlas = SDL_CreateTextureFromSurface( renderer, surface );
    if (image_atlas==NULL){
        printf("Impossible to load %s\n",imgpath);
        exit(1);
    }

    SDL_FreeSurface( surface );
}
/** 
 * 
 * 
 */
void Render(World* world){
    JPoint player_pos;

    /*
      width:world_width = player.box.size.width:x
    */
    
    //Clear screen
    SDL_RenderClear( renderer );

    World_PlayerPosition(world,&player_pos);

    
    Sprite_Move(&player,
                player_pos.x * PIXEL_PER_METER,//(SCREEN_WIDTH/world->size.width),
                player_pos.y * PIXEL_PER_METER);// (SCREEN_HEIGHT/world->size.height));

    //Render texture to screen
    Sprite_Draw(&player,renderer, image_atlas);

    Entity* entity; 
    Sprite* sprite;
    for (int i=0; i<WORLD_MAX_ENTITIES; i++){
        entity = world->entities_pool[i];
        if (entity->life < 0)
            continue;

        switch(entity->type){
        case 1: sprite = &bug;break;
        case 2: sprite = &projectile;break;
        default: sprite = &bug;break;
        }

        Sprite_Move(sprite,
                    entity->box.position.x * PIXEL_PER_METER,
                    entity->box.position.y * PIXEL_PER_METER
                    );
       Sprite_Draw(sprite,renderer,image_atlas);
    }
    //Update screen
    SDL_RenderPresent( renderer );
}

/** 
 * CloseApp
 * 
 */
void CloseApp()
{
    //Free loaded image
    SDL_DestroyTexture(image_atlas);
    image_atlas = NULL;
    
    //Destroy window    
    SDL_DestroyRenderer( renderer );
    SDL_DestroyWindow( window );
    window = NULL;
    renderer = NULL;

    /**/

    //Quit SDL subsystems
    IMG_Quit();
    SDL_Quit();
}

/*******************************************************************************
*                                      MAIN
*******************************************************************************/
int main(int argc, char** argv){

    World world;

    /* calculates world size based on screen size*/
    int wwidth = ((double)SCREEN_WIDTH * 16.94)/64.0;
    int wheight = (int)
        ((double)wwidth / ((double)SCREEN_WIDTH/(double)SCREEN_HEIGHT) );

    PIXEL_PER_METER= (double)SCREEN_WIDTH / (double)wwidth;
    printf("MAIN: world %d, %d\n",wwidth,wheight);

    World_Init(&world,wwidth,wheight,"./media/entities.xml");

    GLboolean running = GL_TRUE;

    /* init sprites from texture atlas */
    GLuint player_frames[4]={0,0,266,488};
    Sprite_Init(&player, 1, player_frames);
    Sprite_Resize_Ratio(&player,PIXEL_PER_METER);

    GLuint bug_frames[4]={268,1,355,158};
    Sprite_Init(&bug, 1, bug_frames);
    Sprite_Resize_Ratio(&bug,PIXEL_PER_METER);

    GLuint projectile_frames[4]={271,161,320,209};
    Sprite_Init(&projectile, 1, projectile_frames);
    Sprite_Resize_Ratio(&projectile, PIXEL_PER_METER);

    /* init SDL2 */
    InitializeSDL();
    LoadMedia();
    SDL_Event event;
    GLuint last_update = SDL_GetTicks();
    
    while (running){
        /* event management */
        while(SDL_PollEvent( &event )) {
            //User requests quit
            if( event.type == SDL_QUIT ) {
                running = GL_FALSE;
            }

            /* gestione tasti */
            //If a key was pressed
            if( event.type == SDL_KEYDOWN){// && event.key.repeat == 0 ) {
                //Adjust the velocity
                switch( event.key.keysym.sym ) {
                case SDLK_UP: world.goUp      = GL_TRUE; break;
                case SDLK_DOWN: world.goDown  = GL_TRUE; break;
                case SDLK_LEFT: world.goLeft  = GL_TRUE; break;
                case SDLK_RIGHT: world.goRight= GL_TRUE; break;
                case SDLK_SPACE: world.Fire   = GL_TRUE; break;

                case SDLK_q:running=GL_FALSE;break;
                }
            }

            else if( event.type == SDL_KEYUP){// && event.key.repeat == 0 ) {
                //Adjust the velocity
                switch( event.key.keysym.sym ) {
                case SDLK_UP: world.goUp      = GL_FALSE; break;
                case SDLK_DOWN: world.goDown  = GL_FALSE; break;
                case SDLK_LEFT: world.goLeft  = GL_FALSE; break;
                case SDLK_RIGHT: world.goRight= GL_FALSE; break;
                case SDLK_SPACE: world.Fire   = GL_FALSE; break;
                }
            }    
        } // end event management

        World_Update(&world,SDL_GetTicks);
        
        GLfloat diff = (SDL_GetTicks() - last_update);
        if (diff <= 16.6666f)
            SDL_Delay(16.6666-diff);

        
        Render(&world);
    }

    /* free memory */
    World_Dealloc(&world);
}
