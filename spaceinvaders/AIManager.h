
#pragma once
#include "JBase.h"
#include "World.h"

typedef GLboolean (*AI_function_pointer) (Entity*,World*,GLfloat);

/** 
 * AIdatabase_Init
 * Initialize a directory of artificial intelligece routines
 * @param ai_directory 
 */
void AI_Init(char* ai_directory);

/** 
 * AI_Execute
 * Execute AI routine relative to e->type
 * @param e 
 * @param w 
 * 
 * @return 
 */
GLboolean AI_Execute(Entity* e, World* w, GLfloat elapsed_time);

GLuint AI_GetRoutineNamed(char* aiprocedure);

/** 
 * AI_Quit
 * Deallocate used memory
 */
void AI_Quit();
