#include "JPool.h"

/** 
 * JPool_New
 */
JPool* JPool_New(GLint size){
    JPool* newpool=malloc(sizeof(JPool));
    newpool->size=size;
    newpool->pool = calloc(size,sizeof(JPointer));
    memset(newpool->pool,0, size*sizeof(JPointer));
    newpool->head = size-1;

    return newpool;
}

/** 
 * JPool_Pop
 */
JPointer JPool_Pop(JPool* jpool){
    if (jpool->head + 1>=jpool->size ){
        return NULL;
    }
    /* pop element and set pointer to null*/
    jpool->head++;
    JPointer popped = jpool->pool[jpool->head];
    if (popped==NULL){
        return NULL;
    }
    jpool->pool[jpool->head] = NULL;

    return popped;
}

/** 
 * JPool_Push
 */
GLint JPool_Push(JPool* jpool, JPointer e){
    /* have you enough space to push a new element? */
    if (jpool->head < 0)
        return -1;
    jpool->pool[jpool->head] = e;
    int last_filled_slot = jpool->head;
    jpool->head--;
    return last_filled_slot;
}

/** 
 * JPool_FreeAll
 */
void JPool_FreeAll(JPool* jpool, void(*DeleteFunc)(JPointer)){

    /* free all allocated slots */
    for (int i=0; i<jpool->size; i++){
        if (jpool->pool[i]!=NULL)
            DeleteFunc(jpool->pool[i]);
    }

    /* free pool struct */
    free(jpool);
}
